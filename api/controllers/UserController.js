/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var Emailaddresses = require('machinepack-emailaddresses');
var Passwords = require('machinepack-passwords');

module.exports = {
    register: function(req, res) {
        if (_.isUndefined(req.param('email'))) {
            return res.json(200, { status: 'error', msg: 'An email address is required!' });
        }

        if (_.isUndefined(req.param('password')) || req.param('password').length < 6) {
            return res.json(200, { status: 'error', msg: 'A password is required, and must be aleast 6 characters' });
        }

        //if (_.isUndefined(req.param('referral_link')) || req.param('referral_link').length < 10) {
        //    return res.json(200, { status: 'error', msg: "The referral link you entered doesn't appear to be valid" });
        //}

        //ReferralService.verifyLink(req.param('referral_link')).then(function(referrer_id) {
            // validate email and password
            Emailaddresses.validate({
                string: req.param('email')
            }).exec({
                error: function (err) {
                    return res.serverError(err);
                },
                invalid: function () {
                    return res.json(200, {status: 'error', msg: "Doesn\'t look like an email address to me!"});
                },
                success: function () {
                    Passwords.encryptPassword({
                        password: req.param('password'),
                    }).exec({
                        error: function (err) {
                            return res.serverError(err);
                        },
                        success: function (encryptedPassword) {
                            // collect ALL signup data
                            var data = {
                                fullname: req.param('fullname'),
                                email: req.param('email'),
                                password: encryptedPassword
                            };

                            User.create(data).exec(function (err, newUser) {
                                if (err) {
                                    if (err.invalidAttributes && err.invalidAttributes.email && err.invalidAttributes.email[0] && err.invalidAttributes.email[0].rule === 'unique') {
                                        return res.json(200, {
                                            status: 'error',
                                            msg: 'Email address is already taken, please try another one.'
                                        });
                                    }
                                    console.log(err);
                                    return res.json(200, {status: 'error', msg: err}); // couldn't be completed
                                }
                                EmailService.sendConfirmationEmail(newUser);
                                //ReferralService.addReferral(newUser.id, referrer_id);
                                return res.json(200, { status: 'success' });
                            });
                        }
                    });
                }
            });
        //}).catch(function(err) {
        //    console.log(err);
        //    return res.json(200, { status: 'error', msg: 'Invalid referral link' });
        //});
    },

    activateAccount: function(req, res) {
        var email = new Buffer(req.param('email'), 'base64').toString('ascii');
        var hash = req.param('hash');
        User.findOne({ email : email }).exec(function(err, user) {
            if (err) return;
            if (user.status == 'Active') {
                return res.view('login', { msg: 'Your email has already been confirmed. Just go ahead and login' });
            }
            if (user) {
                var crypto = require('crypto');
                var confirm_hash = crypto.createHash('md5').update(email + 'okirikwenEE129Okpkenakai').digest('hex');
                if (hash == confirm_hash) {
                    User.update({id: user.id}, {status: 'Active'}).exec(function (err, user) {
                        if (err) {
                            console.log(err);
                        }
                        req.session.userId = user[0].id;
                        req.session.user_type = user[0].user_type;
                        var me = {
                            fname: user[0].fullname.split(' ')[0],
                            lname: user[0].fullname.split(' ')[1]
                        };
                        return res.view('user/profile', {user: user[0], me: me, msg: 'Your email has been confirmed and your account activated.' });
                    });
                } else {
                    return res.ok();
                    console.log('Invalid hash');
                }
            }
        });
    },

    login: function(req, res) {
        User.findOne({ email: req.param('email') }).exec(function(err, foundUser) {
            if (err) return res.json(200, { status: 'Err', msg: err });
            if (!foundUser) return res.json(200, { status: 'Err', msg : 'User not found' });

            if (foundUser.status == 'Active') {
                Passwords.checkPassword({
                    passwordAttempt: req.param('password'),
                    encryptedPassword: foundUser.password
                }).exec({
                    error: function (err) {
                        return res.json(200, {status: 'Err', msg: err});
                    },
                    incorrect: function () {
                        return res.json(200, {status: 'Err', msg: 'User not found'});
                    },
                    success: function () {
                        if (foundUser.deleted) {
                            return res.json(200, {status: 'Err', msg: "'Your account has been deleted.'"});
                        }

                        if (foundUser.banned) {
                            return res.json(200, {
                                status: 'Err',
                                msg: "'Your account has been banned, most likely for violation of the Terms of Service. Please contact us.'"
                            });
                        }
                        req.session.userId = foundUser.id;
                        req.session.fname = foundUser.fullname;
                        return res.json(200, {status: 'success'});
                    }
                });
            } else {
                return res.json(200, {status: 'Err', msg: "'Your account is inactive, please check your email for a confirmation message'"});
            }
        });
    },

    dashboard: function(req, res) {
        if (!req.session.userId) {
            return res.redirect('/login');
        }
        User.find({ id: req.session.userId }).populate('withdrawals').populate('deposits').populate('referrals').exec(function(err, user) {
            if (err) {
                return res.negotiate(err);
            }
            Roi.find({ user: user[0].id }).populate('plan').limit(5).sort('createdAt desc').exec(function(err, rois) {

                var request = require('request');
                request({
                    url: "https://api.blockchain.info/charts/market-price",
                    method: "GET",
                    qs: {timespan: '4weeks', rollingAverage: '8hours', format: 'json'},
                    json: true
                }, function (error, response, body) {
                    //return res.view('user/dashboard', {user: user[0], rois: rois});
                    return res.view('user/dashboard', {user: user[0], rois: rois, market_prices: body.values});
                });
                //return res.view('user/dashboard', {user: user[0], rois: rois});
            })
        });
    },

    signout: function (req, res) {
        if (!req.session.userId) return res.redirect('/');
        User.findOne(req.session.userId, function foundUser(err, createdUser) {
            if (err) return res.negotiate(err);

            if (!createdUser) {
                sails.log.verbose('Session refers to a user who no longer exists.');
                return res.redirect('/');
            }
            req.session.userId = null;
            return res.redirect('/');
        });
    },

    profile: function(req, res) {
        User.findOne(req.session.userId, function(err, _user) {
            var me = {
                fname: _user.fullname.split(' ')[0],
                lname: _user.fullname.split(' ')[1],
            };
            return res.view('user/profile', { user: _user, me: me });
        });
    },

    userPage: function(req, res) {
        User.find({ id: req.param('id') }).populate('deposits').populate('withdrawals').exec(function(err, user) {
            if (err) return;
            return res.view('admin/user-page', { user: user[0] });
        });
    },

    updateProfile: function (req, res) {
        var udata = {
            fullname: req.param('fname') + ' ' + req.param('lname'),
            email: req.param('email'),
            country: req.param('country'),
            phone: req.param('phone')
        };
        User.update({ id: req.session.userId }, udata ).exec(function(err) {
            if (err) {
                return res.negotiate(err);
            }
            return res.ok();
        });
    },

    changePassword: function(req, res) {
        Passwords.encryptPassword({
            password: req.param('password'),
        }).exec({
            error: function(err) {
                return res.serverError(err);
            },
            success: function(encryptedPassword) {
                User.update({ id: req.session.userId }, { password: encryptedPassword }).exec(function() {});
                return res.ok();
            }
        });
    },

    verifyPhone: function(req, res) {
        var phone = req.param('phone');
        // validate phone
        if (phone.charAt(0) == '0') {
            phone = '234' + phone.substr(1);
        } else if (phone.length > 13 || phone.length == 14) {
            phone = '234' + phone.substr(4);
        }
        if (phone.length < 13) {
            return res.json(200, { status: 'Error', msg: 'Invalid phone number' });
        }
        req.session.phone = phone;
        // proceed
        req.session.phone_code = Math.floor(100000 + Math.random() * 900000);
        req.session.save();
        var msg = req.session.phone_code + " is your BitxMining verification code";
        var request = require('request');

        request({
            url: "http://www.multitexter.com/tools/geturl/Sms.php",
            method: "POST",
            qs: { username: 'farmhubb@gmail.com', password: '61761cezeilo', sender: 'BitxMining', message: msg, flash: 1, recipients: phone }
            //json: true
        }, function (err, response, body) {
            return res.json(200, { status: 'success' });
        });
    },

    verifyPhoneCode: function(req, res) {
        var code = req.param('verification_code');
        if (code == req.session.phone_code) {
            User.update({ id: req.session.userId }, { phone_verified: true, phone: req.session.phone }).exec(function(err, user) {

            });
            return res.json(200, { status: 'success', phone: req.session.phone });
        } else {
            return res.json(200, { status: 'error' });
        }
    },


    sendPswdResetEmail: function(req, res) {
        User.find({ email: req.param('email')}).exec(function(err, user) {
            if (user.length > 0) {
                EmailService.sendPswdResetLink(user[0]);
                return res.json(200, { status: 'success' });
            } else {
                return res.json(200, { status: 'error', msg: 'This email is not associated with any account.' });
            }
        });
    },

    showResetPasswordPage: function(req, res) {
        var email = new Buffer(req.param('email'), 'base64').toString('ascii');
        var hash = req.param('hash');

        User.findOne({ email : email }).exec(function(err, user) {
            if (err) {
                return res.badRequest("We don't even know what happened");
            }
            if (user.status == 'Inactive') {
                sendMail.sendConfirmationEmail(newUser);
                return res.view('login', {msg: "You haven\'t verified your email address. Kindly check your email and verify your account"});
            }
            if (user) {
                var crypto = require('crypto');
                var confirm_hash = crypto.createHash('md5').update(email + 'okirikwenEE129Okpkenakai').digest('hex');
                if (hash == confirm_hash) {
                    req.session.user_id = user.id;
                    return res.view('reset-password');
                }
            }
        });
    },

    resetPassword: function(req, res) {
        Passwords.encryptPassword({
            password: req.param('new_password'),
        }).exec({
            error: function (err) {
                return res.json(200, { status: 'error', msg: err });
            },
            success: function (newPassword) {
                User.update({ id: req.session.user_id }, { password: newPassword }).exec(function(err) {
                    if (err) {
                        return res.json(200, { status: 'error', msg: err });
                    }
                    req.session.user_id = null;
                    return res.json(200, { status: 'success' });
                });
            }
        });
    },

    deleteUser: function(req, res) {
        User.destroy({ id: req.param('user_id') }).exec(function() {
            return res.json(200, { status: 'success' });
        });
    }
};

