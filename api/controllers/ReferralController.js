/**
 * ReferralController
 *
 * @description :: Server-side logic for managing Referrals
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    showReferrals: function (req, res) {
        Referral.find({ referrer: req.session.userId }).populate('user').exec(function(err, referrals) {
            if (err) return;
            var ref_code = new Buffer(req.session.userId + '_refcode').toString('base64');
            return res.view('user/referral', { users: referrals, ref_code: ref_code });
        });
    },

    showSignup: function(req, res) {
        //var code = new Buffer(req.param('code'), 'base64').toString('ascii').split('_')[0];
        return res.view('register', { ref_code: req.param('ref_code') });
    }
};

