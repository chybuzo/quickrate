/**
 * WithdrawalController
 *
 * @description :: Server-side logic for managing Withdrawals
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	manageWithdrawals: function(req, res) {
        Withdrawal.find({ user: req.session.userId }).exec(function(err, result) {
            if (err) return console.log(err);
            return res.view('user/withdrawal', { requests: result });
        });
    },


    newRequest: function(req, res) {
        var data = {
            btc: req.param('amount'),
            address: req.param('address'),
            user: req.session.userId
        };
        Withdrawal.create(data).exec(function() {
            return res.redirect('/withdrawal');
        });
    },

    // admin view
    viewWithdrawalRequests: function(req, res) {
        Withdrawal.find({ status: 'Pending' }).populate('user').exec(function(err, result) {
            if (err) return;
            return res.view('admin/withdrawals', { requests: result });
        });
    }
};

