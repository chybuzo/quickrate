/**
 * DepositController
 *
 * @description :: Server-side logic for managing Deposits
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    manageDeposits: function(req, res) {
        Deposit.find({ user: req.session.userId }).populate('plan').sort('createdAt desc').exec(function(err, deposits) {
            if (err) return console.log(err);
            InvestmentPlan.find().exec(function(err, plans) {
                var request = require('request');

                request({
                    url: "https://api.coindesk.com/v1/bpi/currentprice/usd.json",
                    method: "GET"
                }, function (error, response, body) {
                    var result = JSON.parse(body);
                    var x_rate = result.bpi.USD.rate.replace(',', '');
                    return res.view('user/deposit', { deposits: deposits, plans: plans, xrate: result.bpi.USD.rate.split('.')[0], x_rate: x_rate });
                });
                //return res.view('user/deposit', { deposits: deposits, plans: plans });
            });
        });
    },

    addFunds: function(req, res) {
        var data = {
            fiat_amt: req.param('amount'),
            btc: req.param('btc'),
            x_rate: req.param('xchange_rate'),
            address: req.param('btc_address'),
            user: req.session.userId,
            plan: req.param('plan')
        };
        Deposit.create(data).exec(function(err) {
            if (err) console.log(err);
            return res.redirect('/deposit');
        });
    },

    addHash: function(req, res) {
        Deposit.update({ id: req.param('deposit_id') }, { tnx_hash: req.param('tnx_hash'), status: 'Processing' }).exec(function() {
            return res.json(200, { status: 'success' });
        });
    },

    confirmDeposit: function(req, res) {
        Deposit.update({ id: req.param('id') }, { status: 'Confirmed' }).exec(function() {
            return res.json(200, { status: 'success' });
        });
    },

    getHash: function(req, res) {
        Deposit.find({ id: req.param('id') }).exec(function(err, deposit) {
            if (err) return;
            return res.json(200, { status: 'success', hash: deposit[0].tnx_hash });
        });
    },

    // admin view
    viewDeposits: function(req, res) {
        Deposit.find().populate('user').populate('plan').sort('createdAt desc').exec(function(err, deposits) {
            if (err) return res.badRequest();
            return res.view('admin/deposits', { deposits: deposits });
        });
    }
};

