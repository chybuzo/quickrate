module.exports = {
    addReferral: function(new_user, referrer_id) {
        return new Promise(function(resolve, reject) {
            var data = {
                user: new_user,
                referrer: referrer_id
            };
            console.log(data);
            Referral.create(data).exec(function () {
                return resolve(true);
            });
        });
    },

    verifyLink: function(referral_link) {
        return new Promise(function(resolve, reject) {
            var code = referral_link.split('/').pop();
            var referrer = new Buffer(code.toString(), 'base64').toString('ascii');
            if (referrer.split('_')[0] + '_refcode' != referrer) {
                return reject('Invalid referral link.');
            }
            User.find({ id: referrer.split('_')[0] }).exec(function(err, user) {
                if (err) return reject(false);
                if (user.length > 0) {
                    return resolve(user[0].id);
                } else {
                    return reject(false);
                }
            });
        });
    }
}