'use strict';
const nodemailer = require('nodemailer');
var hbs = require('nodemailer-express-handlebars');
var options = {
    viewEngine: 'express-handlebars',
    viewPath: sails.config.appPath + '/views/emails/'
};

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
    host: 'smtp.zoho.com',
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
        user: 'noreply@btc-empower.com',
        pass: 'unknown12'  // generated ethereal password
    }
});
transporter.use('compile', hbs(options));

module.exports = {
    sendConfirmationEmail: function(user) {
        var email_b64 = new Buffer(user.email).toString('base64');
        var crypto = require('crypto');
        var hash = crypto.createHash('md5').update(user.email + 'okirikwenEE129Okpkenakai').digest('hex');

        let mailOptions = {
            from: '"BTC Empower" <noreply@btc-empower.com>', // sender address
            to: user.fullname + ', ' + user.email,
            subject: 'Btc Empower - Email Confirmation', // Subject line
            template: 'confirmation',
            context: {
                name: user.fullname,
                email: email_b64,
                hash: hash
            }
        };
        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
        });
    },


    sendPswdResetLink: function(user) {
        var email_b64 = new Buffer(user.email).toString('base64');
        var crypto = require('crypto');
        var hash = crypto.createHash('md5').update(user.email + 'okirikwenEE129Okpkenakai').digest('hex');

        let mailOptions = {
            from: '"Btc Empower" <noreply@btc-empower.com>', // sender address
            to: user.fullname + ', ' + user.email,
            subject: 'Btc Empower - Password Reset', // Subject line
            template: 'passwordReset',
            context: {
                user: user.fullname,
                url: 'https://btc-empower.com/user/resetpassword/' + email_b64 + '/' + hash
            }
        };
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            //console.log('Message sent: %s', info.messageId);
        });
    },
}