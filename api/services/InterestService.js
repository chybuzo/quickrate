var Big = require('big.js');

var rate, roi;
const maintenance_fee = 0.0002; // $$

module.exports = {

    giveDailyProfit: function() {
        Deposit.find({ status: 'Confirmed' }).populate('plan').exec(function(err, deposits) {
            //console.log('Confirmed Deposits...');
            if (err) return res.badRequest();
            deposits.forEach(function(deposit) {
                var from = new Date(deposit.updateAt);
                var today = new Date();
                var oneDay = 24*60*60*1000;

                var diffDays = Math.round(Math.abs((from.getTime() - today.getTime())/(oneDay)));
                console.log(diffDays);

                var profit = module.exports.dailyInterest(deposit);
                var data = {
                    user: deposit.user,
                    plan: deposit.plan,
                    deposit: deposit.id,
                    profit: profit.roi,
                    rate: profit.rate,
                    maintenance_fee: maintenance_fee
                };
                Roi.create(data).exec(function() {});
            });
        });
    },


    dailyInterest: function(deposit) {
        //console.log('Plan: ' + deposit.plan.title);
        switch (deposit.plan.title) {
            case 'Silver':
                rate = module.exports.randomRate(0.6, 0.72);
                break;
            case 'Gold':
                rate = module.exports.randomRate(0.72, 0.83);
                break;
            case 'Platinum':
                rate = module.exports.randomRate(0.83, 0.86);
                break;
            case 'Diamond':
                rate = module.exports.randomRate(0.86, 0.94);
                break;
            Default:
                break;
        }
        //console.log('daily interest');
        //console.log({
        //    roi: ((parseFloat(deposit.fiat_amt) * (rate / 100)) - maintenance_fee).toFixed(2),
        //    rate: rate
        //});
        return {
            roi: (parseFloat(deposit.fiat_amt) * (parseFloat(rate) / 100)) - maintenance_fee,
            rate: rate
        };
    },


    randomRate: function(min, max) {
        min = new Big(min);
        max = new Big(max);
        var rand = new Big(Math.random());
        return parseFloat((max.minus(min).times(rand).plus(min).toFixed(2)).toString());
    }

    //randomRate: function(min, max) {
    //    return Math.random() * (max - min + 1) + min;
    //}
}