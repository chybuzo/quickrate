/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        fullname: {
          type: 'string'
        },
    
        email: {
          type: 'string',
          email: 'true',
          unique: 'true'
        },

        country: {
            type: 'string'
        },
    
        phone: {
          type: 'string'
        },
    
        password: {
          type: 'string'
        },

        referrer: {
            model: 'user'
        },

        referrals: {
            collection: 'referral',
            via: 'referrer'
        },

        withdrawals: {
            collection: 'withdrawal',
            via: 'user'
        },

        deposits: {
            collection: 'deposit',
            via: 'user'
        },

        phone_verified: {
            type: 'boolean',
            defaultsTo: 'false'
        },
    
        deleted: {
          type: 'boolean'
        },
    
        banned: {
          type: 'boolean'
        },

        status: {
            type: 'string',
            defaultsTo: 'Inactive'
        }
    }
};

