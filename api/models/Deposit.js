/**
 * Deposit.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    attributes: {
        fiat_amt: {
            type: 'integer'
        },

        currency: {
            type: 'string'
        },

        btc: {
            type: 'float'
        },

        x_rate: {
            type: 'float'
        },

        address: {
            type: 'string'
        },

        tnx_hash: {
            type: 'string'
        },

        user: {
            model: 'user'
        },

        plan: {
            model: 'investmentplan'
        },

        status: {
            type: 'string',
            defaultsTo: 'Pending'
        }
    }
};

