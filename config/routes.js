module.exports.routes = {

    '/': {
        view: 'index'
    },

    'GET /login': {
        view: 'login'
    },

    'GET /register': {
        view: 'register'
    },

    'POST /register': 'UserController.register',

    'POST /login': 'UserController.login',

    'GET /logout': 'UserController.signout',

    'GET /user/dashboard': 'UserController.dashboard',

    'GET /user/profile': 'UserController.profile',

    'POST /user/updateprofile': 'UserController.updateProfile',

    'POST /user/changepassword': 'UserController.changePassword',

    'POST /user/sendPswdResetEmail': 'UserController.sendPswdResetEmail',

    'POST /user/resetpassword': 'UserController.resetPassword',

    'GET /user/resetpassword/:email/:hash': 'UserController.showResetPasswordPage',

    'GET /user/activate/:email/:hash': 'UserController.activateAccount',

    'GET /user/referral': 'ReferralController.showReferrals',

    'POST /user/delete': 'UserController.deleteUser',

    'POST /user/addphone': 'UserController.verifyPhone',

    'POST /user/verify-code': 'UserController.verifyPhoneCode',

    'GET /referral_link/:ref_code': 'ReferralController.showSignup',

    'GET /deposit': 'DepositController.manageDeposits',

    'POST /deposit/save': 'DepositController.addFunds',

    'POST /deposit/addhash': 'DepositController.addHash',

    'POST /deposit/confirm': 'DepositController.confirmDeposit',

    'GET /deposit/gethash/:id': 'DepositController.getHash',

    'GET /withdrawal': 'WithdrawalController.manageWithdrawals',

    'POST /withdrawal/newrequest': 'WithdrawalController.newRequest',

    'GET /sendmail': 'TestController.sendMail',

    'GET /admin': { view: 'admin/login' },

    'GET /admin/create': { view: 'admin/create' },

    'POST /admin/create': 'AdminController.addAdmin',

    'POST /admin/login': 'AdminController.login',

    'GET /admin/users': 'AdminController.showUsers',

    'GET /admin/signout': 'AdminController.signout',

    'GET /admin/withdrawals': 'WithdrawalController.viewWithdrawalRequests',

    'GET /admin/deposits': 'DepositController.viewDeposits',

    'GET /admin/userpage/:id': 'UserController.userPage',


    'GET /privacypolicy': { view: 'privacy' },

    'GET /termsofuse': { view: 'termsofuse' },

    'GET /faq': { view: 'faq'},

    'GET /viewinvestments': 'TestController.viewROI'

};
